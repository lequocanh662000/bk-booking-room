# Hotel Booking
Prerequisite:
- Docker Engine
- Docker compose 
- Stop service mysql if it's running
To stop it run:
```
sudo service mysql stop 
```

# Steps to initialize database
1. Initialize "ass" database and create tables
```
docker compose up -d
```

2. Use SQL client software application(such as: DBeaver https://dbeaver.io/) to connect to "ass" database
in port 3306
user: root
password: P@ssw0rd

3. Run script data.sql to insert data into tables.

4. Run script procedure.sql or trigger.sql if u want to test them. 