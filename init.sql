DROP DATABASE IF EXISTS ass;

CREATE DATABASE IF NOT EXISTS ass;

USE ass;

-- 1
CREATE TABLE Branch (
  id varchar(6) PRIMARY KEY,
  province varchar(255) NOT NULL,
  address varchar(255) NOT NULL,
  phone varchar(11),
  email varchar(255)
);

-- GENERATING Branch_id
CREATE TABLE Branch_seq (
  id int NOT NULL AUTO_INCREMENT PRIMARY KEY
);

delimiter |
CREATE TRIGGER Branch_id_gen
BEFORE INSERT ON Branch
FOR EACH ROW
BEGIN
  INSERT INTO Branch_seq VALUES (NULL);
  SET NEW.id = CONCAT('CN', LAST_INSERT_ID());
END;
|
delimiter ;
-- END: GENERATING Branch_id

-- 2
CREATE TABLE Branch_image (
  branch_id varchar(6),
  image varchar(255),
  PRIMARY KEY (branch_id, image)
);

-- 3
CREATE TABLE Zone (
  branch_id varchar(6),
  name varchar(255),
  PRIMARY KEY (branch_id, name)
);

-- 4
CREATE TABLE Room_type (
  id int PRIMARY KEY AUTO_INCREMENT,
  name varchar(255),
  area float COMMENT 'Unit: meter square',
  description varchar(255) DEFAULT NULL,
  customer_no int NOT NULL
	CHECK (customer_no >= 1 AND customer_no <= 10)
);

-- 5
CREATE TABLE Bed (
  room_type_id int,
  size decimal(2,1),
  number int NOT NULL DEFAULT 1
	CHECK (number >= 1 AND number <= 10),
  PRIMARY KEY (room_type_id, size)
);

-- 6
CREATE TABLE Branch_room_type (
  room_type_id int,
  branch_id varchar(6),
  price int NOT NULL COMMENT 'Unit: 1000VND',
  PRIMARY KEY (room_type_id, branch_id)
);

-- 7
CREATE TABLE Room (
  branch_id varchar(6),
  code char(3),
  zone_name varchar(255),
  room_type_id int,
  PRIMARY KEY (branch_id, code)
);

-- 8
CREATE TABLE Supply_type (
  id char(6) PRIMARY KEY COMMENT 'VT[0-9][0-9][0-9][0-9]',
  name varchar(255) NOT NULL
);

-- 9
CREATE TABLE Room_type_supply_type (
  room_type_id int,
  supply_type_id char(6),
  number int NOT NULL DEFAULT 1
	CHECK (number >= 1 AND number <= 20),
  PRIMARY KEY (supply_type_id, room_type_id)
);

-- 10
CREATE TABLE Supply (
  branch_id varchar(6),
  room_code char(3),
  supply_type_id char(6),
  id int CHECK (id > 0),
  status varchar(255),
  PRIMARY KEY (branch_id, supply_type_id, id)
);

-- 11
CREATE TABLE Supply_provider (
  id char(7) PRIMARY KEY COMMENT 'NCC[0-9][0-9][0-9][0-9]',
  name varchar(255) UNIQUE,
  email varchar(255),
  address varchar(255)
);

-- 12
CREATE TABLE Supply_providing (
  supply_provider_id char(7),
  branch_id varchar(6),
  supply_type_id char(6),
  PRIMARY KEY (branch_id, supply_type_id)
);

-- 13
CREATE TABLE Customer (
  id char(8) PRIMARY KEY COMMENT 'in form of KH[0-9][0-9][0-9][0-9][0-9][0-9]',
  ssn varchar(12) UNIQUE NOT NULL,
  phone varchar(11) UNIQUE NOT NULL,
  email varchar(255) UNIQUE,
  username varchar(255) UNIQUE,
  point int NOT NULL DEFAULT 0 CHECK (point >= 0),
  customer_rank int DEFAULT 1 COMMENT '1-potential, 2-loyal, 3-vip, 4-supervip'
	CHECK (customer_rank >= 1 AND customer_rank <= 4)
);

-- 14
CREATE TABLE Package (
  name varchar(255) PRIMARY KEY,
  total_day int NOT NULL
	CHECK (total_day >= 1 AND total_day <= 100),
  total_customer int NOT NULL
	CHECK (total_customer >= 1 AND total_customer <= 10),
  price int NOT NULL COMMENT 'Unit: 1000VND'
);

-- 15
CREATE TABLE Package_order (
  customer_id char(8),
  package_name varchar(255),
  buy_time timestamp DEFAULT current_timestamp,
  start_date date NOT NULL COMMENT 'this package will expired 1 year after start_date',
  cost int NOT NULL COMMENT 'Unit: 1000VND',
  PRIMARY KEY (customer_id, package_name, buy_time),
  CHECK (start_date >= DATE(buy_time))
);

-- 16
CREATE TABLE Room_order (
  id char(16) PRIMARY KEY,
  order_time timestamp NOT NULL DEFAULT current_timestamp,
  checkin_date date NOT NULL,
  checkout_date date NOT NULL,
  status int NOT NULL DEFAULT 0 COMMENT '0-unpaid, 1-paid, 2-cancel not refund yet, 3-cancel refund already'
	  CHECK (status >= 0 AND status <= 3),
  cost int NOT NULL DEFAULT 0 COMMENT 'Unit: 1000VND',
  customer_id char(8),
  package_name varchar(255),
  CHECK (checkin_date > DATE(order_time) AND checkout_date > checkin_date)
);

-- GENERATING Room_order_id
CREATE TABLE Room_order_seq (
  id int NOT NULL AUTO_INCREMENT PRIMARY KEY
);

delimiter |
CREATE TRIGGER Room_order_id_gen
BEFORE INSERT ON Room_order
FOR EACH ROW
BEGIN
  INSERT INTO Room_order_seq VALUES (NULL);
  SET NEW.id = CONCAT('DP', REPLACE(DATE(NEW.order_time), '-', ''), LPAD(LAST_INSERT_ID(), 6, '0'));
END;
|
delimiter ;
-- END: GENERATING Room_order_id

-- 17
CREATE TABLE Rent_room (
  room_order_id char(16),
  branch_id varchar(6),
  room_code char(3),
  PRIMARY KEY (room_order_id, branch_id, room_code)
);

-- 18
CREATE TABLE Receipt (
  id varchar(255) PRIMARY KEY COMMENT 'in form of DP[DDMMYYYY][6-digit incremental int_num]',
  checkin_time time NOT NULL COMMENT 'must be greater than booking_date',
  checkout_time time NOT NULL COMMENT 'must be greater than checkin_date',
  room_order_id char(16)
);

-- GENERATING Receipt_id
CREATE TABLE Receipt_seq (
  id int NOT NULL AUTO_INCREMENT PRIMARY KEY
);

delimiter |
CREATE TRIGGER Receipt_id_gen
BEFORE INSERT ON Receipt
FOR EACH ROW
BEGIN
  INSERT INTO Receipt_seq VALUES (NULL);
  SET NEW.id = CONCAT('HD', REPLACE(DATE(current_timestamp), '-', ''), LPAD(LAST_INSERT_ID(), 6, '0'));
END;
|
delimiter ;
-- END: GENERATING Receipt_id

-- 19
CREATE TABLE Company (
  id char(6) PRIMARY KEY COMMENT 'in form of DN[0-9][0-9][0-9][0-9]',
  name varchar(255) UNIQUE NOT NULL
);

-- 20
CREATE TABLE Service (
  id char(6) PRIMARY KEY COMMENT 'in form of DV[R|S|C|M|B][0-9][0-9][0-9]',
  type char(1) NOT NULL,
  company_id char(6)
);


-- 21
CREATE TABLE Spa_product (
  service_id char(6),
  product varchar(255),
  PRIMARY KEY (service_id, product)
);

-- 22
CREATE TABLE Souvenir_type (
  service_id char(6),
  type varchar(255),
  PRIMARY KEY (service_id, type)
);

-- 23
CREATE TABLE Souvenir_brand (
  service_id char(6),
  brand varchar(255),
  PRIMARY KEY (service_id, brand)
);

-- 24
CREATE TABLE Premises (
  branch_id varchar(6),
  code int NOT NULL DEFAULT 1
    CHECK (code >= 1 AND code <= 50),
  length int,
  width int,
  price int NOT NULL,
  description varchar(255),
  zone_name varchar(255),
  service_id char(6) DEFAULT NULL,
  store_name varchar(255) DEFAULT NULL,
  logo varchar(255) DEFAULT NULL,
  PRIMARY KEY (branch_id, code)
);

-- 25    
CREATE TABLE Premises_image (
  image varchar(255),
  branch_id varchar(6),
  premises_code int,
  PRIMARY KEY (branch_id, premises_code, image)
);

-- 26
CREATE TABLE Premises_active_hour (
  branch_id varchar(6),
  premises_code int,
  open_time time,
  close_time time,
  PRIMARY KEY (branch_id, premises_code, open_time, close_time)
);

-- 27
CREATE TABLE Restaurant (
  service_id char(6) PRIMARY KEY,
  customer_no int,
  style varchar(255)
);

ALTER TABLE Branch_image ADD FOREIGN KEY (branch_id) REFERENCES Branch (id)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE Zone ADD FOREIGN KEY (branch_id) REFERENCES Branch (id)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE Bed ADD FOREIGN KEY (room_type_id) REFERENCES Room_type (id)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE Branch_room_type 
ADD FOREIGN KEY (room_type_id) REFERENCES Room_type (id)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD FOREIGN KEY (branch_id) REFERENCES Branch (id)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE Room 
ADD FOREIGN KEY (branch_id) REFERENCES Branch (id)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD FOREIGN KEY (room_type_id) REFERENCES Room_type (id)
  ON DELETE SET NULL
  ON UPDATE CASCADE,
ADD FOREIGN KEY (branch_id, zone_name) REFERENCES Zone (branch_id, name)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE Room_type_supply_type ADD FOREIGN KEY (supply_type_id) REFERENCES Supply_type (id)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD FOREIGN KEY (room_type_id) REFERENCES Room_type (id)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE Supply 
ADD FOREIGN KEY (branch_id) REFERENCES Branch (id)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD FOREIGN KEY (supply_type_id) REFERENCES Supply_type (id)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD FOREIGN KEY (branch_id, room_code) REFERENCES Room (branch_id, code)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE Supply_providing 
ADD FOREIGN KEY (supply_provider_id) REFERENCES Supply_provider (id)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD FOREIGN KEY (branch_id) REFERENCES Branch (id)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD FOREIGN KEY (supply_type_id) REFERENCES Supply_type (id)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE Package_order 
ADD FOREIGN KEY (customer_id) REFERENCES Customer (id)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD FOREIGN KEY (package_name) REFERENCES Package (name)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE Room_order 
ADD FOREIGN KEY (customer_id) REFERENCES Customer (id)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD FOREIGN KEY (package_name) REFERENCES Package_order (package_name)
  ON DELETE SET NULL
  ON UPDATE CASCADE;

ALTER TABLE Rent_room 
ADD FOREIGN KEY (room_order_id) REFERENCES Room_order (id)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD FOREIGN KEY (branch_id, room_code) REFERENCES Room (branch_id, code)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE Receipt ADD FOREIGN KEY (room_order_id) REFERENCES Room_order (id)
  ON DELETE CASCADE
  ON UPDATE CASCADE;
    
ALTER TABLE Premises 
ADD FOREIGN KEY (branch_id) REFERENCES Branch (id)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD FOREIGN KEY (branch_id, zone_name) REFERENCES Zone (branch_id, name)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD FOREIGN KEY (service_id) REFERENCES Service (id)
  ON DELETE SET NULL
  ON UPDATE CASCADE;
    
ALTER TABLE Premises_image
ADD FOREIGN KEY (branch_id, premises_code) REFERENCES Premises (branch_id, code)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE Premises_active_hour ADD FOREIGN KEY (branch_id, premises_code) REFERENCES Premises (branch_id, code)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE Service ADD FOREIGN KEY (company_id) REFERENCES Company (id)
  ON DELETE CASCADE
  ON UPDATE CASCADE;
    
ALTER TABLE Restaurant ADD FOREIGN KEY (service_id) REFERENCES Service (id)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE Spa_product ADD FOREIGN KEY (service_id) REFERENCES Service (id)
  ON DELETE CASCADE
  ON UPDATE CASCADE;
    
ALTER TABLE Souvenir_type ADD FOREIGN KEY (service_id) REFERENCES Service (id)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE Souvenir_brand ADD FOREIGN KEY (service_id) REFERENCES Service (id)
  ON DELETE CASCADE
  ON UPDATE CASCADE;